Redmine::Plugin.register :sal_pay do
  name 'Invoice'
  author 'Karma wangchuk'
  description 'Calculates the total spend time on a project and displays the amount in USD,YEN and NU. Exports and converts the data in invoice format to pdf'
  version '1.1.0'
  menu :application_menu, :sal_pay, { controller: 'salaries', action: 'index' }, caption: 'Invoice'
end
