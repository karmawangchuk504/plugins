class CreateSalaries < ActiveRecord::Migration[5.2]
  def change
    create_table :salaries do |t|
      t.integer :Project
      t.integer :Cost_per_hour
      t.date :Date_from
      t.date :Date_to
    end
  end
end
