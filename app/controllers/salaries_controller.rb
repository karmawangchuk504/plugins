class SalariesController < ApplicationController

  def index
  	@salary = Salary.new
  	@time_t = TimeEntry.all
  	@pro = Project.all
    if Salary.exists?
      @project_name = Salary.last.Project
      @project_year = Salary.last.Date_from
      @project_month = Salary.last.Date_to
      @cost = Salary.last.Cost_per_hour
      @count =0
      #search for the values through project and date
      @time_t.each do |tt|
        if tt.project_id == @project_name && tt.spent_on.between?(@project_year,@project_month) 
          @count=@count+tt.hours  
        end
      end
      @count1 = @count*@cost
      @count2 = @count*@cost*0.02
      @count3 = @count*@cost*1.45

      #Replace the name of the project in the table
      @pro.each do |pp|
        if pp.id ==@project_name
          @name = pp.name
          break
        end
      end

#Initial Value
    else
      @count = "Null"
      @count1 = "Null"
      @count2 = "Null"
      @count3 = "Null"
      @name = "Null"
      @cost = "Null"
    end
    # PDF converter
       respond_to do |format|
      	format.html
      	format.pdf do
	      	pdf = MemberPdf.new(@name,@count,@cost,@count1,@count2,@count3)
	      	send_data pdf.render, filename: 'member.pdf', type: 'application/pdf',disposition: "inline"
      	end   
      end
  end

  def create
    @salary = Salary.new(salary_params)
    @salary.save
    redirect_to :back
    

  end

  private
  def salary_params
    params.require(:salary).permit(:Project, :Cost_per_hour, :Date_from, :Date_to)
  end
  end


