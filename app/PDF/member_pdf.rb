class MemberPdf < Prawn::Document
	require 'numbers_in_words'
	def initialize(names,count,cost,count1,count2,count3)
		super()
		@name= names
		@count= count
		@cost=cost
		@count1=count1
		@count2=count2
		@count3=count3
		@number = NumbersInWords.in_words(@count2)
		in_voices
		invoice_table
		database
	end

	def in_voices
		image "#{Rails.root}/plugins/sal_pay/app/images/logo.jpg", :at => [0,730], :width => 120
		text "#{Date.today}" , :style => :bold , :size => 12 , :align => :right
		move_down 10 
		text "<b><u>Invoice</u></b>",:align => :center, :inline_format => true, :size => 20
		move_down 30
		text "TIJ-Tech Private Limited",:align => :center, :inline_format => true
		text "Serbithang,Thimphu Tech Park Ltd. Block 2",:align => :center, :inline_format => true
		move_down 110
		text "TO, Peace Growba Co.,LTD",:align => :left, :inline_format => true
		
	end

	def invoice_table
		move_down 20
		table line_items,:position => :left  do 
			row(0).font_style = :bold
			row(0).background_color = "b7b5b0"
			row(1).background_color = "b7b5b0"
        	row(0).width = 90
        end
        move_down 100
        text "<i>In words: USD #{@number}</i> ",:align => :center, :size => 12, :inline_format => true
        move_down 250
        text "Customer's signature",:align => :left, :inline_format => true


	end

	def line_items
		[[{:content => "Project", :rowspan => 2},{:content => "Working hrs", :rowspan => 2},{:content => "Cost/hrs", :rowspan => 2},{:content => "Amount", :colspan => 3}],
		["Yen","USD","Nu"],
		[@name,@count,@cost,@count1,@count2,@count3]]
	end
	#clear database
	def database
    Salary.delete_all
    end

end
